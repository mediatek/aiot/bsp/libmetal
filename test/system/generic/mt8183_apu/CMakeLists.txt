collect (PROJECT_LIB_TESTS helper.c)

set_property (GLOBAL PROPERTY TEST_LINKER_OPTIONS "-mlsp=${XTENSA_LSP}")

collect(PROJECT_LIB_DEPS c)
collect(PROJECT_LIB_DEPS m)
