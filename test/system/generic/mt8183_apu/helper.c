/*
 * Copyright (c) 2020, BayLibre
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <metal/irq.h>
#include <metal/sys.h>

#include "rsc_table.h"

int init_system(void)
{
	int ret;

	/* Initialize the logger */
	resource_table_log_init();

	ret = mt8183_irq_init();
	if (ret)
		return ret;

	return 0;
}

void cleanup_system()
{
}
