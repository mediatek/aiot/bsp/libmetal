//  Memory map file to generate linker scripts for programs without board I/O.

// Customer ID=12842; Build=0x69682; Copyright (c) 2004-2015 Cadence Design Systems, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


// A memory map is a sequence of memory descriptions and
// optional parameter assignments.
//
// Each memory description has the following format:
//   BEGIN <name>
//     <addr> [,<paddr>] : <mem-type> : <mem-name> : <size> [,<psize>]
//                       : [writable] [,executable] [,device] ;
//     <segment>*
//   END <name>
//
// where each <segment> description has the following format:
//     <seg-name> : F|C : <start-addr> - <end-addr> [ : STACK ] [ : HEAP ]
//                : <section-name>* ;
//
// Each parameter assignment is a keyword/value pair in the following format:
//   <keyword> = <value>                (no spaces in <value>)
// or
//   <keyword> = "<value>"              (spaces allowed in <value>)
//
// The following primitives are also defined:
//   PLACE SECTIONS( <section-name>* ) { WITH_SECTION(<section-name>)
//                                       | IN_SEGMENT(<seg-name>) }
//
//   NOLOAD <section-name1> [ <section-name2> ... ]
//
// Please refer to the Xtensa LSP Reference Manual for more details.
//
VECRESET = 0x50000000
VECBASE = 0x60000000

BEGIN srom
0x50000000: sysrom : srom :0x1000000: executable ;
 srom0 : F : 0x50000000 - 0x500002ff : .ResetVector.text;
 srom1 : C : 0x50000300 - 0x501fffff : .srom.rodata .srom.literal .srom.text .rom.store;
END srom

BEGIN sram
0x60000000: sysram : sram :0x10000000: executable, writable ;
 sram0 : F : 0x60000000 - 0x60000177 : .WindowVectors.text;
 sram1 : F : 0x60000178 - 0x6000017f : .Level2InterruptVector.literal;
 sram2 : F : 0x60000180 - 0x600001b7 : .Level2InterruptVector.text;
 sram3 : F : 0x600001b8 - 0x600001bf : .DebugExceptionVector.literal;
 sram4 : F : 0x600001c0 - 0x600001f7 : .DebugExceptionVector.text;
 sram5 : F : 0x600001f8 - 0x600001ff : .NMIExceptionVector.literal;
 sram6 : F : 0x60000200 - 0x60000237 : .NMIExceptionVector.text;
 sram7 : F : 0x60000238 - 0x6000023f : .KernelExceptionVector.literal;
 sram8 : F : 0x60000240 - 0x60000277 : .KernelExceptionVector.text;
 sram9 : F : 0x60000278 - 0x6000027f : .UserExceptionVector.literal;
 sram10 : F : 0x60000280 - 0x600002b7 : .UserExceptionVector.text;
 sram11 : F : 0x600002b8 - 0x600002ff : .DoubleExceptionVector.literal;
 sram12 : F : 0x60000300 - 0x6000033f : .DoubleExceptionVector.text;
 sram13 : C : 0x60000340 - 0x600fffff : .resource_table .sram.rodata .clib.rodata .rtos.rodata .rodata .sram.literal .literal .rtos.literal .clib.literal .sram.text .text .clib.text .rtos.text .clib.data .clib.percpu.data .rtos.percpu.data .rtos.data .sram.data .data __llvm_prf_names .clib.bss .clib.percpu.bss .rtos.percpu.bss .rtos.bss .sram.bss .bss;
sram14 : C : 0x60110000 - 0x6020ffff : .vdev;
sram15 : C : 0x60100000 - 0x60107fff : .vring0;
sram16 : C : 0x60108000 - 0x6010ffff : .vring1;
sram17 : C : 0x60210000 - 0x6030ffff : .log;
sram18 : C : 0x60410000 - 0x6080ffff :  HEAP :;
sram19 : C : 0x60310000 - 0x6040ffff :  STACK :;
END sram

BEGIN dram0
0x7ff00000: dataRam : dram0 : 0x20000 : writable ;
 dram0_0 : C : 0x7ff00000 - 0x7ff1ffff : .dram0.rodata .dram0.literal .dram0.data .dram0.bss;
END dram0

BEGIN dram1
0x7ff20000: dataRam : dram1 : 0x20000 : writable ;
 dram1_0 : C : 0x7ff20000 - 0x7ff3ffff : .dram1.rodata .dram1.literal .dram1.data .dram1.bss;
END dram1

BEGIN iram0
0x7ff40000: instRam : iram0 : 0x40000 : executable, writable ;
 iram0_0 : C : 0x7ff40000 - 0x7ff7ffff : .iram0.literal .iram0.text;
END iram0

