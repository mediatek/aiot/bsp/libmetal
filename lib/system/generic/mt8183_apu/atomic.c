/*
 * Copyright (c) 2020, BayLibre
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

/*
 * @file	generic/mt8183_apu/atomic.c
 * @brief	machine specific atomic primitives implementation.
 */

#include <metal/atomic.h>
#include <metal/irq.h>

#include <stdint.h>
#include <string.h>

uint32_t __atomic_exchange_4(uint32_t *ptr, uint32_t val, int memorder)
{
	uint32_t ret;
	unsigned int flags;

	flags = metal_irq_save_disable();

	(void) memorder;
	ret = *ptr;
	*ptr = val;
	metal_irq_restore_enable(flags);

	return ret;
}

uint32_t __atomic_fetch_add_4(uint32_t *ptr, uint32_t val, int memorder)
{
	uint32_t ret;
	unsigned int flags;

	flags = metal_irq_save_disable();

	(void) memorder;
	ret = *ptr;
	*ptr += val;
	metal_irq_restore_enable(flags);

	return ret;
}
