collect (PROJECT_LIB_HEADERS irq.h)
collect (PROJECT_LIB_HEADERS sys.h)

collect (PROJECT_LIB_SOURCES atomic.c)
collect (PROJECT_LIB_SOURCES irq.c)
collect (PROJECT_LIB_SOURCES log.c)
collect (PROJECT_LIB_SOURCES sys.c)

# vim: expandtab:ts=2:sw=2:smartindent
